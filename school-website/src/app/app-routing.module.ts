import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditDetailsComponent } from './edit-details/edit-details.component';
import { EndingPageComponent } from './ending-page/ending-page.component';
import { ForThanksComponent } from './for-thanks/for-thanks.component';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginComponent } from './login/login.component';
import { StuNewAdminComponent } from './stu-new-admin/stu-new-admin.component';
import { StudentListComponent } from './student-list/student-list.component';
import { VerifyDetailsComponent } from './verify-details/verify-details.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { CreateNewPasswordComponent } from './forgot-password/create-new-password.component';


const routes: Routes = [
  { path: '',   redirectTo: '/welcomeTo-High-school', pathMatch: 'full' }, 
  {path: "welcomeTo-High-school", component:WelcomeComponent},
  {path: "login", component:LoginComponent},
  {path: "home-page",component:HomePageComponent},
  {path: 'for-thanks',component:ForThanksComponent},
  {path: "student-data", component:StudentListComponent},
  { path: "edit-detail/:id",component :EditDetailsComponent},
  { path: "new-Addmision", component:StuNewAdminComponent},
  { path : 'verify-details',component:VerifyDetailsComponent},
  {path: 'admission-done',component:EndingPageComponent},
  {path: 'login-page',component:LoginComponent},
  {path: 'forgot-password',component:ForgotPasswordComponent},
  {path: 'create-new-password',component:CreateNewPasswordComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

})
export class AppRoutingModule { }
