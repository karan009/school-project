import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-verify-details',
  templateUrl: './verify-details.component.html',
  styleUrls: ['./verify-details.component.scss'],
})
export class VerifyDetailsComponent implements OnInit {
  isLinear=false;
  showData: any;
  form: FormGroup;
  constructor(private fb: FormBuilder, private route: Router, private snackbar:MatSnackBar) {
    this.form = this.fb.group({
      name: [''],
      lname:[''],
      childAge:[''],
      stuClass:[''],
      stuNo:[''],
      stuEmail:[''],
      stuDob:[''],
      stuPinCode:[''],
      newLocation:[''],
      fname:[''],
      fAge:[''],
      fEmail:[''],
      mname:[''],
      mAge:[''],
      mEmail:[''],
      parentNumber:[''],
      LandLineNumber:[''],
      pPinCode:[''],
      pAdders:[''],
      parentDob:[''],
    });
  }

  ngOnInit(): void {
    var store = localStorage.getItem('inputVal');
    if (store) {
      let data = JSON.parse(store);
      this.showData = data;
      this.form.setValue({
        name: this.showData.firstname,
        lname:this.showData.lastname,
        childAge:this.showData.age,
        stuClass:this.showData.class,
        stuNo:this.showData.phoneNo,
        stuEmail:this.showData.email,
        stuDob:this.showData.dob,
        stuPinCode:this.showData.pincode,
        newLocation:this.showData.adders,
        fname:this.showData.fathername,
        fAge:this.showData.fatherAge,
        fEmail:this.showData.fatherEmail,
        mname:this.showData.mothername,
        mAge:this.showData.motherAge,
        mEmail:this.showData.motherEmail,
        parentDob:this.showData.fatherdob,
        parentNumber:this.showData.contect,
        LandLineNumber:this.showData.houseNumber,
        pAdders:this.showData.parentadders,
        pPinCode:this.showData.parentpincode,
      });
    }
  }
  submitForm() {
    this.snackbar.open("Thanks  To You 🙏 For Submiting Form ",'',{duration:2000}) ;
    setTimeout(() => {
      this.route.navigate(['admission-done']);
    }, 2000);
  }
}
