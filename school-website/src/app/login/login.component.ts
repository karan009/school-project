import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  // public 
  reactiveForm: any;
  passwordGet: any;
  hide = true;
  showData: any;
  showPassword: any;
  // username="";
  // password="";
  constructor(
    private fb: FormBuilder,
    private route: Router,
    private snackBar: MatSnackBar
  ) {
    this.reactiveForm = this.fb.group({
      Emails: ['',Validators.required],
     
    });
    this.passwordGet=this.fb.group({
      PassWord: ['',Validators.required]  ,
    })
  }

  ngOnInit(): void {
   this.getEmail();
   this.getPassword();
    
  }
  clicked(eve: any) {
    console.log(':: eve', eve);
    if(this.reactiveForm.invalid && this.passwordGet.invalid){
      this.snackBar.open("Invalid username or password","",{duration:2000})
      if(this.reactiveForm==Error && this.passwordGet==Error){
        this.snackBar.open("Invalid username or password","",{duration:2000})
      } 
    }else{
      this.route.navigate(['/for-thanks'])
    }
    localStorage.removeItem("email");
    localStorage.removeItem("password");

    //   var username=this.reactiveForm.get('username')?.value;
    //   var password=this.reactiveForm.get('password')?.value;
    //   this.service.getUserPass(username,password).subscribe((resp:any)=>{
    //   if(resp.status==="Error" && this.reactiveForm.invalid){
    //     this.snackBar.open('Invalid UserName || Password',"",{
    //       duration:2000
    //     })
    //   }else{
    //     this.route.navigate(["/for-thanks"])
    //   }
    // localStorage.setItem(resp,JSON.stringify(resp));
    // })
  }
  getEmail(){
    var getEmail = localStorage.getItem('email');
    if (getEmail) {
      var store = JSON.parse(getEmail);
      this.showData = store;
      this.reactiveForm.setValue({
        Emails: this.showData.email,
      });
    }
  }
  getPassword(){
    var getPassword = localStorage.getItem('password');
    if (getPassword) {
      var password = JSON.parse(getPassword);
      this.showPassword = password;
      this.passwordGet.setValue({
        PassWord: this.showPassword.reNewPassword,
      });
    }
  }
}
