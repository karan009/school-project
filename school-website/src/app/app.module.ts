import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule} from '@angular/material/icon';
import {  HttpClientModule } from '@angular/common/http';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import { MatOptionModule, MatPseudoCheckboxModule } from '@angular/material/core';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { CardsComponent } from './cards/cards.component';
import { ForThanksComponent } from './for-thanks/for-thanks.component';
import { StuNewAdminComponent } from './stu-new-admin/stu-new-admin.component';
import { StudentListComponent } from './student-list/student-list.component';
import { VerifyDetailsComponent } from './verify-details/verify-details.component';
import { EndingPageComponent } from './ending-page/ending-page.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { EditDetailsComponent } from './edit-details/edit-details.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { CreateNewPasswordComponent } from './forgot-password/create-new-password.component';
import { ModelDialogComponent } from './forgot-password/dialog-box/model-dialog.component';
import { TryComponent } from './try/try.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    CardsComponent,
    ForThanksComponent,
    StuNewAdminComponent,
    StudentListComponent,
    VerifyDetailsComponent,
    EndingPageComponent,
    WelcomeComponent,
    EditDetailsComponent,
    HomePageComponent,
    ForgotPasswordComponent,
    CreateNewPasswordComponent,
    ModelDialogComponent,
    TryComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatOptionModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatRadioModule, 
    MatSelectModule,
    MatSnackBarModule,
    MatPseudoCheckboxModule,
    MatDialogModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
