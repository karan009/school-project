import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-details',
  templateUrl: './edit-details.component.html',
  styleUrls: ['./edit-details.component.scss'],
})
export class EditDetailsComponent implements OnInit {
  inputForm: any;
  cardId:any;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private http:HttpClient,
    private snackBar:MatSnackBar,
    private routes:Router
  ) {
    this.inputForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      id: ['', Validators.required],
      age:['',Validators.required],
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe((res: any) => {
     this.cardId= res.id;
      console.log(this.cardId,":: card id")
      this.http.get
        ('http://localhost:3000/student-data/'+this.cardId)
        .subscribe((respon: any) => {
          var response= respon;
          console.log(response, ':: response');
          this.inputForm.setValue({          
            firstName: response.first_name,
            lastName:response.last_name,
            email:response.email,
            id: response.id,  
            age:response.age,
          });
        });
    });
  }
  saveChangs(){
    var validdation=this.inputForm.invalid ? (  this.snackBar.open("Invalid Form","",{ duration:2000 })) :(this.routes.navigate(["/student-data"]), this.snackBar.open("Edit Form Successfully","",{ duration:2000 }))
  }
}
