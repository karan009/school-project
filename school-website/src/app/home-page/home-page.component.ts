import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
interface Grade{
value:string;
grades:string;
}
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  grade: Grade[]=[
    {value: 'grade-0',grades:'Nursrey'},
    {value: 'grade-1',grades:'KG '},
    {value: 'grade-2',grades:'Grade 1'},
    {value: 'grade-3',grades:'Grade 2'},
    {value: 'grade-4',grades:'Grade 3'},
    {value: 'grade-5',grades:'Grade 4'},
    {value: 'grade-6',grades:'Grade 5'},
    {value: 'grade-7',grades:'Grade 6'},
  ]
  reactiveform: any;
  constructor(private fb: FormBuilder,private route:Router, private snackbar:MatSnackBar) {
    this.reactiveform = this.fb.group({
      firstname: new FormControl( '',
      [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(10),
      ],),
      lastname: new FormControl("",
      [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(10),
      ],),
      email: ['', Validators.required],
      number: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
    });
  }

  ngOnInit(): void {}
  knowMore(){
    if(this.reactiveform.invalid){
      this.snackbar.open("Please Fill Form","",{duration:2000})
    }else{
        this.route.navigate(['/for-thanks'])
    }
  }
}
