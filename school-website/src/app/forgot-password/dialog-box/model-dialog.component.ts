import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
@Component({
  selector: 'app-model-dialog',
  templateUrl: './model-dialog.component.html',
  styleUrls: ['./model-dialog.component.scss'],
})
export class ModelDialogComponent {
  constructor(
    private route: Router,
    public dialogRef: MatDialogRef<ModelDialogComponent>
  ) {}
  ngOnInit(): void {}
  submit() {
    this.route.navigate(['login-page']);
    this.dialogRef.close();
  }        
}
