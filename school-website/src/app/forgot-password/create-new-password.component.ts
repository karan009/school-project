import { JsonPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ModelDialogComponent } from './dialog-box/model-dialog.component';

@Component({
  selector: 'app-create-new-password',
  templateUrl: './create-new-password.component.html',
  styleUrls: ['./create-new-password.component.scss'],
})
export class CreateNewPasswordComponent implements OnInit {
  createPasword: any;
  hide = true;
  oldhide = true;
  Rehide = true;
  
  ngOnInit(): void {}
  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private snackbar: MatSnackBar
  ) {
    this.createPasword = this.fb.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      reNewPassword: ['', Validators.required],
    });
  }
  Submit() {
    var newPassword =this.createPasword.get('newPassword').value;
    var reNewPassword =this.createPasword.get('reNewPassword').value;
    var confirmPassword= newPassword === reNewPassword && this.createPasword.valid ? this.passwordMatch() :  this.snackbar.open('Password not Match', '', { duration: 2000 });

  }
  passwordMatch(){
    var setItem = JSON.stringify(this.createPasword.value);
     localStorage.setItem('password', setItem);
    var dialogRef = this.dialog.open(ModelDialogComponent, {
      width: '250px',
    });
  }
}
