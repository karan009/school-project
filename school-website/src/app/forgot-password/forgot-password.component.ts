import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPassword:any;
  forGetPassword:boolean=true;
  constructor(private fb:FormBuilder,private snackBar:MatSnackBar,private route:Router) {
    this.forgotPassword=this.fb.group({
   email:["",Validators.required],
   phone:["",[Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
    })
   }

  ngOnInit(): void {
  }
  createPassword(){
    var setItem=JSON.stringify(this.forgotPassword.value);
    localStorage.setItem('email',setItem)
    if(this.forgotPassword.invalid){
      this.snackBar.open("Invalid Field",'',{
        duration:2000
      })
    }else{
      setTimeout(()=>{
        this.route.navigate(['create-new-password'])
      },2000)
    }
  }

}
