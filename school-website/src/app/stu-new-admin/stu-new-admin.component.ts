import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
@Component({
  selector: 'app-stu-new-admin',
  templateUrl: './stu-new-admin.component.html',
  styleUrls: ['./stu-new-admin.component.scss'],
})
export class StuNewAdminComponent implements OnInit {
  Admission: FormGroup;
  selectCountry: any;
  availableStates: any;
  availiableCity: any;

  data = {
    countries: [
      {
        CountryName: 'India',
        State: [
          {
            state: 'Punjab',
            city: ['Amritsar', 'Jalandhar', 'Ludhiana', 'Batala', 'Mohali'],
          },
          {
            state: 'Haryana',
            city: ['Faridabad', 'Gurugram', 'Panipat', 'Amabala'],
          },
          {
            state: 'kernatka',
            city: ['Bengaluru', 'Hubballi-Dhawad', 'Mysuru', 'Davangree'],
          },
          {
            state: 'Delhi',
            city: [
              'Tughlqabad',
              'Jahanpanah',
              'Firozobad',
              'Shahjahanabad',
              'New Delhi',
            ],
          },
          {
            state: 'Assam',
            city: ['Guwahati', 'Silchar', 'Dibrugarh'],
          },
        ],
      },
      {
        CountryName: 'Canada',
        State: [
          {
            state: 'Alberta',
            city: ['Calgary', 'Edmonton', 'Lethbridge', 'Spruce Grove'],
          },
          {
            state: ' British Columbia',
            city: ['Vancouver', 'Surrey', 'Saanich', 'Richmond', 'Burnaby'],
          },
          {
            state: 'Ontario',
            city: ['Toronto', 'Vaughan', 'Brampton'],
          },
        ],
      },
      {
        CountryName: 'USA',
        State: [
          {
            state: 'California',
            city: ['Los Angeles', 'San Francisco', 'Santa Maria'],
          },
          {
            state: 'New Mexico',
            city: ['Santa Fe', 'North Valley', 'Bernalillo'],
          },
          {
            state: 'New Jersey',
            city: ['Newark', 'Jersey City', 'Paterson', 'Elizabeth'],
          },
        ],
      },
      {
        CountryName: 'Russia',
        State: [
          {
            state: 'Kaluga',
            city: ['Ulyanovo', 'Vorotynsk', 'Kurovskoy', 'Pyatovskiy'],
          },
        ],
      },
      {
        CountryName: 'Bangladesh',
        State: [
          {
            state: 'Barisal',
          },
        ],
      },
    ],
  };

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private route: Router
  ) {
    this.Admission = this.fb.group({
      firstname: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(10),
        ],
      ],
      lastname: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(10),
        ],
      ],
      email: ['', Validators.required],
      age: [' ', [Validators.required, Validators.minLength(2)]],
      class: ['', Validators.required],
      phoneNo: [
        '',
        [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')],
      ],
      contect: [
        '',
        [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')],
      ],
      houseNumber: [
        '',
        [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')],
      ],
      dob: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      pincode: [
        '',
        [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{6}$')],
      ],
      adders: ['', Validators.required],
      fathername: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(10),
        ],
      ],
      mothername: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(10),
        ],
      ],
      fatherEmail: ['', Validators.required],
      motherEmail: ['', Validators.required],
      fatherAge: [
        ' ',
        [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{2}$')],
      ],
      motherAge: [
        ' ',
        [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{2}$')],
      ],
      fatherdob: ['', Validators.required],
      parentadders: ['', Validators.required],
      parentpincode: [
        '',
        [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{6}$')],
      ],
      Proof:["",Validators.required]
    });
  }

  ngOnInit(): void {}
  proof = [
    {
      BirthCard: 'Birth Card',
      Passport: 'Passport',
      AddharCard: 'Addhar Card',
      PanCard: 'panCard',
    },
  ];
  submitForm(event: any) {
    var storeVal = JSON.stringify(this.Admission.value);
    localStorage.setItem('inputVal', storeVal);
    if (this.Admission.invalid) {
      this.snackBar.open('Incorrect Form Values', '', {
        duration: 2000,
      });
    } else {
      this.route.navigate(['verify-details']);
    }
  }
  getCountry() {
    let countryName = this.Admission.get('country')?.value;
    this.selectCountry = this.data.countries.filter(
      (country) => country.CountryName == countryName
    )[0];
    this.availableStates = this.selectCountry.State;
  }
  getStates() {
    let stateName = this.Admission.get('state')?.value;
    var getState = this.availableStates.filter(
      (state: any) => state.state == stateName
    )[0];
    this.availiableCity = getState.city;
  }
}
