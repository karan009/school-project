import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StuNewAdminComponent } from './stu-new-admin.component';

describe('StuNewAdminComponent', () => {
  let component: StuNewAdminComponent;
  let fixture: ComponentFixture<StuNewAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StuNewAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StuNewAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
