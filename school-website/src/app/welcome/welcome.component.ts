import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  constructor(private route:Router) { }

  ngOnInit(): void {
  }
  staffClick(){
    this.route.navigate(['login'])

  }
  newAdmin(){
  this.route.navigate(['home-page'])  
  }
}
