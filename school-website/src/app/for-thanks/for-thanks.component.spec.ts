import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForThanksComponent } from './for-thanks.component';

describe('ForThanksComponent', () => {
  let component: ForThanksComponent;
  let fixture: ComponentFixture<ForThanksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForThanksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForThanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
