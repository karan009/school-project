import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.scss']
})
export class StudentListComponent implements OnInit {
  constructor(private route:Router) { }
  date=new Date()
store:any;
storee:any;
studentData= [{
  "first_name": "Karan ",
  "last_name": "Sharma",
  "email": "Karan.procloudware@email.com",
  "age": 22,
  "id": 1,
  "profile": "https://us.123rf.com/450wm/adsniks/adsniks1906/adsniks190600106/124419182-cute-little-indian-indian-asian-school-boy-wearing-uniform.jpg?ver=6"
},
{
  "first_name": "Tobias ",
  "last_name": "Funke",
  "email": "tobias.funke@email.in",
  "age": 24,
  "id": 2,
  "profile": "https://previews.123rf.com/images/lightpoet/lightpoet1203/lightpoet120300026/12637664-in-the-library-pretty-female-student-with-laptop-and-books-working-in-a-high-school-library.jpg?fj=1"
},
{
  "first_name": "Lindsay ",
  "last_name": "Ferguson",
  "email": "lindsay.ferguson@email.in",
  "age": 25,
  "id": 3,
  "profile": "https://previews.123rf.com/images/phase4/phase40908/phase4090800365/5413278-young-woman-reading-a-book-at-school.jpg?fj=1"
},
{
  "first_name": "Michael ",
  "last_name": "Lawson",
  "email": "michael.lawson@email.in",
  "age": 25,
  "id": 4,
  "profile": "https://previews.123rf.com/images/lightpoet/lightpoet1303/lightpoet130300030/18609241-.jpg?fj=1"
},
{
  "first_name": "Byron ",
  "last_name": "Fields",
  "email": "byron.fields@email.in",
  "age": 25,
  "id": 5,
  "profile": "https://previews.123rf.com/images/adsniks/adsniks1908/adsniks190800660/128478315-handsome-indian-asian-college-male-student-carrying-bag.jpg?fj=1"
},
{
  "first_name": "George ",
  "last_name": "Edwards",
  "email": "george.edwards@email.in",
  "age": 23,
  "id": 6,
  "profile": "https://previews.123rf.com/images/michaeljung/michaeljung1011/michaeljung101100038/8196945-smart-junior-high-student-studying-at-desk.jpg?fj=1"
},
{
  "first_name": "Rachel ",
  "last_name": "Howell",
  "email": "rachel.howell@email.in",
  "age": 22,
  "id": 7,
  "profile": "https://reqres.in/img/faces/7-image.jpg"
},
{
  "first_name": "Byron",
  "last_name": "Edwards",
  "email": "Byron.edwards@email.in",
  "age": 21,
  "id": 8,
  "profile": "https://reqres.in/img/faces/8-image.jpg"
},
{
  "first_name": "Leanne",
  "last_name": "Graham",
  "email": "Sincere@email.com",
  "age": 24,
  "id": 9,
  "profile": "http://www.nysed.gov/common/nysed/files/bigstock-high-school-student-at-desk-in-62219015.jpg"
},
{
  "first_name": "Ervin",
  "last_name": "Howell",
  "email": "Shanna@email.com",
  "age": 20,
  "id": 10,
  "profile": "https://www.thebalancecareers.com/thmb/hfXLen7lTCN62ggBijspA19wKYA=/300x200/filters:no_upscale():max_bytes(150000):strip_icc():saturation(0.2):brightness(10):contrast(5):format(webp)/attractive-young-woman-at-the-library-chin-on-hand-524165792-57265f823df78ced1fd9eb6a.jpg"
},
{
  "first_name": "Clementine ",
  "last_name": "Bauch",
  "email": "Nathan@email.com",
  "age": 25,
  "id": 11,
  "profile": "https://previews.123rf.com/images/belchonock/belchonock1903/belchonock190313598/119969977-african-american-teenage-boy-with-headphones-using-laptop-at-table-in-room.jpg?fj=1"
},
{
  "first_name": "Patricia  ",
  "last_name": "Lebsack",
  "email": "Julianne.OConner@email.com",
  "age": 23,
  "id": 12,
  "profile": "https://reqres.in/img/faces/12-image.jpg"
},
{
  "first_name": "Chelsey   ",
  "last_name": "Dietrich",
  "email": "Lucio_Hettinger@email.com",
  "age": 19,
  "id": 13,
  "profile": "https://previews.123rf.com/images/langstrup/langstrup1708/langstrup170800081/83471565-young-african-female-entrepreneur-smiling-confidently-while-riding-up-an-escalator-in-the-lobby-of-a.jpg?fj=1"
},
{
  "first_name": "Dennis",
  "last_name": "Schulist",
  "email": "Karley_Dach@email.com",
  "age": 24,
  "id": 14,
  "profile": "https://reqres.in/img/faces/10-image.jpg"
},
{
  "first_name": "Kurtis",
  "last_name": "Weissnat",
  "email": "Telly.Hoeger@email.com",
  "age": 26,
  "id": 15,
  "profile": "https://reqres.in/img/faces/11-image.jpg"
},
{
  "first_name": "Nicholas",
  "last_name": "Runolfsdottir",
  "email": "Sherwood@email.com",
  "age": 20,
  "id": 16,
  "profile": "https://reqres.in/img/faces/9-image.jpg"
}
]

  ngOnInit(): void {
    this.store=this.studentData;
  }
  editDetail(id:any){
     this.route.navigate(["/edit-detail/",id])
  }
}
